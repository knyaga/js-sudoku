var express = require('express');
var cors = require('cors');

var sudokuController = require('./controllers/sudoku-controller');

var app = express();

var PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + '/../public'));
app.use(cors());

app.get('/sudoku', sudokuController);


app.listen(PORT, function() {
    console.log('Server runnning at http://localhost:' + PORT);
})