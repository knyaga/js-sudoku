var fs = require('fs');

function readSudokuTextFile(callback) {

    var path = __dirname + '/../data/sudoku.txt';
    var readableStream = fs.createReadStream(path, { encoding: 'utf8' });

    readableStream.on('data', function(fileContent) {
        callback(null, fileContent);
    });

    readableStream.on('error', function(error) {
        callback(error, null);
    });
}

function generate2DArray(file) {
    return file.split('\n').map(function(row) {
        return row.split('').map(function(number) {
            return +number;
        })
    });
}

var sudokuController = function(req, res) {
    readSudokuTextFile(function(error, file) {
        if (!error) {
            var convertedArray = generate2DArray(file);
            res.status(200).send(convertedArray);
        }
    });
}

module.exports = sudokuController;