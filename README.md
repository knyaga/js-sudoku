##Requirement
This project was built using:
1. Node.js version v6.11.2

##How to run the project
1. Git clone the repo: from your terminal run `git clone https://knyaga@bitbucket.org/knyaga/js-sudoku.git`
2. cd to downloaded folder: js-sudoku
3. Run `npm instal` from root of your downloaded folder
4. Run `npm run dev` and navigate to `http://localhost:3000`
