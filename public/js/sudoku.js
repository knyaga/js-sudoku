var sudoku = (function($) {
    "use strict";

    var sudokuArray = fetchSudoku();

    function fetchSudoku() {
        return $.ajax({
            url: 'http://localhost:3000/sudoku',
            method: 'GET',
            headers: { "Accept": "application/json" },
            async: false,
            error: function(error) {
                console.log(error);
            }
        }).responseJSON;
    }

    function sudokuModelView() {
        for (var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                var cellId = "#R" + r + "C" + c;
                if (sudokuArray[r][c] !== 0) {
                    $(cellId).html(sudokuArray[r][c]);
                }
            }
        }
    }

    function validateRow(row, value) {
        for (var col = 0; col < 9; col++) {
            if (sudokuArray[row][col] === value) {
                return false;
            }
        }
        return true;
    }

    function validateColumn(col, value) {
        for (var row = 0; row < 9; row++) {
            if (sudokuArray[row][col] === value) {
                return false;
            }
        }
        return true;
    }

    function validate3x3Grid(row, col, value) {
        var blockRow = parseInt(row / 3) * 3;
        var blockCol = parseInt(col / 3) * 3;

        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                if (sudokuArray[blockRow + i][blockCol + j] === value) {
                    return false;
                }
            }
        }
        return true;
    }

    function solveSudoku(row, col) {
        if (row > 8) {
            console.log('Solution found');
            sudokuModelView();
            return;
        }
        if (sudokuArray[row][col] !== 0)
            next(row, col);
        else {
            for (var num = 1; num < 10; num++) {
                if (validateRow(row, num) &&
                    validateColumn(col, num) &&
                    validate3x3Grid(row, col, num)) {

                    sudokuArray[row][col] = num;
                    next(row, col);
                }
            }
            sudokuArray[row][col] = 0;
        }
    }

    function next(row, col) {
        if (col < 8) {
            solveSudoku(row, col + 1);
        } else {
            solveSudoku(row + 1, 0);
        }
    }

    return {
        init: sudokuModelView,
        solve: solveSudoku
    }
})($);